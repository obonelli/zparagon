sap.ui.define(
  [
    'sap/ui/core/mvc/Controller',
    'sap/m/MessageBox',
    './utilities',
    'sap/ui/core/routing/History',
    'sap/ui/model/json/JSONModel',
  ],
  function (BaseController, MessageBox, Utilities, History, JSONModel) {
    'use strict';

    return BaseController.extend(
      'com.sap.build.standard.zobx_p28_kitsc.controller.Page1',
      {
        getSearchFilter: function () {
          let $filter = '';

          //this.getView().byId('filterMaterial').getS

          return $filter;
        },

        onTableItemPress: function (oEvent) {
          debugger;
          let model = this.getOwnerComponent().getModel('items');
          if (!model) {
            this.getOwnerComponent().setModel(
              new JSONModel({
                Werks: '',
                Lgort: '',
                KitMatnr: '',
              }),
              'items'
            );
            model = this.getOwnerComponent().getModel('items');
          }

          let item = oEvent
            .getParameter('listItem')
            .getBindingContext()
            .getObject();
          model.setData(item);
          this.oRouter.navTo('Page2', {
            context: Math.random(),
          });
        },

        populateLocalFilters: function (data) {
          // Initialize filters arrays
          for (let filter of this.filters) {
            filter = [];
          }

          // Populate filters arrays
          for (let item of data) {
            for (let filterName in this.filters) {
              let filter = this.filters[filterName];
              let val = item[filterName];
              if (!filter.includes(val)) {
                filter.push(val);
              }
            }
          }

          // Update filters models
          for (let filterName in this.filters) {
            let filterData = this.filters[filterName].map((e) => {
              return {
                value: e,
              };
            });
            this.getView()
              .byId(`filter${filterName}`)
              .setModel(
                new JSONModel({
                  FilterSet: filterData,
                })
              );
          }
        },

        onSearch: function () {
          sap.ui.core.BusyIndicator.show();
          let $filter = this.getSearchFilter();
          var url = `${this.serviceUrl}/ZACV_C_KIT_HEADER?${$filter}`;
          $.ajax({
            type: 'GET',
            url: url,
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Accept', 'application/json');
            },
            context: this,
            dataType: 'json',
            success: function (res) {
              //let data = [];

              var data = res.d.results;
              console.log(data);
              //var temps = data;
              //Status A = Active P = Partial I = Inactive
              for (const [key, value] of Object.entries(data)) {
                if (value.Status == 'A') {
                  value.Status = 'Active';
                }
                if (value.Status == 'P') {
                  value.Status = 'Partial';
                }
                if (value.Status == 'I') {
                  value.Status = 'Inactive';
                }
              }
              console.log(data);

              /**/

              // Aplanar result
              // for (let i in res.d.results) {
              //   let el = res.d.results[i];
              //   if (!data.length) {
              //     data.push(el);
              //   } else {
              //     let found = false;
              //     for (let it of data) {
              //       if (JSON.stringify(it) === JSON.stringify(el)) {
              //         found = true;
              //         break;
              //       }
              //     }
              //     if (!found) {
              //       data.push(el);
              //     }
              //   }
              // }

              // Obtener filtros actuales y filtrar
              let selectedMatItems = this.getView()
                .byId('filterMaterial')
                .getSelectedItems();
              let selectedMaterials = selectedMatItems.map((e) => e.getText());

              let selectedCompMatItems = this.getView()
                .byId('filterComponentMaterial')
                .getSelectedItems();
              let selectedCompMaterials = selectedCompMatItems.map((e) =>
                e.getText()
              );

              let selectedBatchItems = this.getView()
                .byId('filterBatch')
                .getSelectedItems();
              let selectedBatchs = selectedBatchItems.map((e) => e.getText());

              let selectedPlantItems = this.getView()
                .byId('filterPlant')
                .getSelectedItems();
              let selectedPlants = selectedPlantItems.map((e) => e.getText());

              let selectedStorageLocationItems = this.getView()
                .byId('filterSlocation')
                .getSelectedItems();
              let selectedStorageLocation = selectedStorageLocationItems.map((e) => e.getText());

              let selectedStatusItems = this.getView()
                .byId('filterStatus')
                .getSelectedItems();
              let selectedStatus = selectedStatusItems.map((e) => e.getText());

              let tableData;

              if (selectedMaterials.length || selectedCompMaterials.length || selectedBatchItems.length
                || selectedPlantItems.length || selectedStorageLocationItems.length || selectedStatusItems.length) {
                tableData = data.filter((e) => {
                  return (
                    selectedMaterials.includes(e.Material) ||
                    selectedCompMaterials.includes(e.ComponentMaterial) ||
                    selectedBatchs.includes(e.Batch) ||
                    selectedPlants.includes(e.Plant) ||
                    selectedStorageLocation.includes(e.Slocation) ||
                    selectedStatus.includes(e.Status)
                  );
                });
              } else {
                tableData = data;
              }

              /**/

              const aModel = new sap.ui.model.json.JSONModel({
                SalesOrderSet: tableData,
              });
              aModel.setSizeLimit(tableData.length);

              this.getView().byId('tableItems').setModel(aModel);
              //this.populateLocalFilters(data);
              sap.ui.core.BusyIndicator.hide();
            },
            error: function (err) {
              sap.ui.core.BusyIndicator.hide();
              const details =
                (err && err.message && err.message.value) ||
                'Error not defined.';
              MessageBox.error('Error', {
                title: 'Error found searching',
                details,
              });
            },
          });
        },

        formatLeadingZeros: function (n) {
          return n?.replace(/^0+/, '') || '';
        },

        formatDate: function (date) {
          let formattedFechaEntrega = '';
          if (date) {
            const formatter = sap.ui.core.format.DateFormat.getDateInstance({
              pattern: 'dd/MM/YYYY',
            });
            formattedFechaEntrega = formatter.format(new Date(date));
          }
          return formattedFechaEntrega;
        },

        handleRouteMatched: function (oEvent) {
          /*
          var sAppId = 'App60a2a3ba0a52d66331ca971e';

          var oParams = {};

          if (oEvent.mParameters.data.context) {
            this.sContext = oEvent.mParameters.data.context;
          } else {
            if (this.getOwnerComponent().getComponentData()) {
              var patternConvert = function (oParam) {
                if (Object.keys(oParam).length !== 0) {
                  for (var prop in oParam) {
                    if (prop !== 'sourcePrototype' && prop.includes('Set')) {
                      return prop + '(' + oParam[prop][0] + ')';
                    }
                  }
                }
              };

              this.sContext = patternConvert(
                this.getOwnerComponent().getComponentData().startupParameters
              );
            }
          }

          var oPath;

          if (this.sContext) {
            oPath = {
              path: '/' + this.sContext,
              parameters: oParams,
            };
            this.getView().bindObject(oPath);
          }
          */
          //this.onSearch();
        },
        _onFioriListReportTableUpdateFinished: function (oEvent) {
          var oTable = oEvent.getSource();
          var oHeaderbar = oTable.getAggregation('headerToolbar');
          if (oHeaderbar && oHeaderbar.getAggregation('content')[1]) {
            var oTitle = oHeaderbar.getAggregation('content')[1];
            if (
              oTable.getBinding('items') &&
              oTable.getBinding('items').isLengthFinal()
            ) {
              oTitle.setText(
                '(' + oTable.getBinding('items').getLength() + ')'
              );
            } else {
              oTitle.setText('(1)');
            }
          }
        },
        onInit: function () {
          this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
          this.oRouter
            .getTarget('Page1')
            .attachDisplay(jQuery.proxy(this.handleRouteMatched, this));

          this.serviceUrl = `/sap/opu/odata/sap/ZACV_C_KIT_HEADER_CDS`;
          var url = `${this.serviceUrl}/ZACV_C_KIT_HEADER`;
          var urlMaterial = `${this.serviceUrl}/I_Material`;

          this.filters = {
            Material: [],
            Batch: [],
            Plant: [],
            Slocation: [],
            Status: [],
            ComponentMaterial: [],
            ComponentBatch: [],
          };

          $.ajax({
            type: 'GET',
            url: url,
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Accept', 'application/json');
            },
            context: this,
            dataType: 'json',
            success: function (res) {

              var data = res.d.results;
              //var temps = data;
              for (const [key, value] of Object.entries(data)) {
                //console.log(key);
                //console.log(value.Batch);
                if (value.Status == 'A') {
                  value.Status = 'Active';
                  //console.log(key, value);
                  //temps = key, value;
                  //console.log(value.Status);
                }
                if (value.Status == 'P') {
                  value.Status = 'Partial';
                }
                if (value.Status == 'I') {
                  value.Status = 'Inactive';
                }
              }
              //console.log(temps);
              console.log(data);
              //Se llenan los combos al iniciar la app
              function onlyUnique(value, index, self) {
                return self.indexOf(value) === index;
              }

              let batchnumbers = data
                .map((e) => {
                  return e.Batch;
                })
                .filter(onlyUnique)
                .map((e, i) => {
                  return {
                    key: i,
                    value: e,
                  };
                });
              this.getView()
                .byId('filterBatch')
                .setModel(
                  new sap.ui.model.json.JSONModel({
                    FilterSet: batchnumbers,
                  })
                );

              let materials = data
                .map((e) => {
                  return e.Material;
                })
                .filter(onlyUnique)
                .map((e, i) => {
                  return {
                    key: i,
                    value: e,
                  };
                });
              this.getView()
                .byId('filterMaterial')
                .setModel(
                  new sap.ui.model.json.JSONModel({
                    FilterSet: materials,
                  })
                );

              let plants = data
                .map((e) => {
                  return e.Plant;
                })
                .filter(onlyUnique)
                .map((e, i) => {
                  return {
                    key: i,
                    value: e,
                  };
                });
              this.getView()
                .byId('filterPlant')
                .setModel(
                  new sap.ui.model.json.JSONModel({
                    FilterSet: plants,
                  })
                );

              let slocations = data
                .map((e) => {
                  return e.Slocation;
                })
                .filter(onlyUnique)
                .map((e, i) => {
                  return {
                    key: i,
                    value: e,
                  };
                });
              this.getView()
                .byId('filterSlocation')
                .setModel(
                  new sap.ui.model.json.JSONModel({
                    FilterSet: slocations,
                  })
                );

              let status = data
                .map((e) => {
                  return e.Status;
                })
                .filter(onlyUnique)
                .map((e, i) => {
                  return {
                    key: i,
                    value: e,
                  };
                });
              this.getView()
                .byId('filterStatus')
                .setModel(
                  new sap.ui.model.json.JSONModel({
                    FilterSet: status,
                  })
                );

              ////

            },
            error: function (err) {
              sap.ui.core.BusyIndicator.hide();
              const details =
                (err && err.message && err.message.value) ||
                'Error not defined.';
              MessageBox.error('Error', {
                title: 'Error found searching',
                details,
              });
            },
          });

          $.ajax({
            type: 'GET',
            url: urlMaterial,
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Accept', 'application/json');
            },
            context: this,
            dataType: 'json',
            success: function (res) {

              let dataMaterial = res.d.results;
              //console.log(dataMaterial);
              //Se llenan los combos al iniciar la app
              function onlyUnique(value, index, self) {
                return self.indexOf(value) === index;
              }

              // let materials = dataMaterial
              //   .map((e) => {
              //     return e.Material;
              //   })
              //   .filter(onlyUnique)
              //   .map((e, i) => {
              //     return {
              //       key: i,
              //       value: e,
              //     };
              //   });
              // this.getView()
              //   .byId('filterMaterial')
              //   .setModel(
              //     new sap.ui.model.json.JSONModel({
              //       FilterSet: materials,
              //     })
              //   );

              // let compMaterials = dataMaterial
              //   .map((e) => {
              //     return e.MaterialType;
              //   })
              //   .filter(onlyUnique)
              //   .map((e, i) => {
              //     return {
              //       key: i,
              //       value: e,
              //     };
              //   });
              // this.getView()
              //   .byId('filterComponentMaterial')
              //   .setModel(
              //     new sap.ui.model.json.JSONModel({
              //       FilterSet: compMaterials,
              //     })
              //   );
              //////

            },
            error: function (err) {
              sap.ui.core.BusyIndicator.hide();
              const details =
                (err && err.message && err.message.value) ||
                'Error not defined.';
              MessageBox.error('Error', {
                title: 'Error found searching',
                details,
              });
            },
          });
        },
        onExit: function () {
          // to destroy templates for bound aggregations when templateShareable is true on exit to prevent duplicateId issue
          var aControls = [
            {
              controlId:
                'Fiori_ListReport_ListReport_0-content-Fiori_ListReport_Table-1',
              groups: ['items'],
            },
          ];
          for (var i = 0; i < aControls.length; i++) {
            var oControl = this.getView().byId(aControls[i].controlId);
            if (oControl) {
              for (var j = 0; j < aControls[i].groups.length; j++) {
                var sAggregationName = aControls[i].groups[j];
                var oBindingInfo = oControl.getBindingInfo(sAggregationName);
                if (oBindingInfo) {
                  var oTemplate = oBindingInfo.template;
                  oTemplate.destroy();
                }
              }
            }
          }
        },
      }
    );
  },
  /* bExport= */ true
);
